package com.example.demoRest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class DemoController {

	@GetMapping("/demo")
	public String holaMundo() {
		return "Hola Mundo";
	}
}
